#!./ENV/bin/python3
import pyAesCrypt
import click
import os
import hashlib
import zipfile

BUFFER_SIZE = 64 * 1024
NON_EXISTENT = 'null'
CRYPT_EXT = '.aes'
SHA256SUM_EXT = '.sha256sum'
ARCHIVE_EXT = '.zip'


@click.group()
def main():
    """Simple program that encrypt or decrypt files."""
    pass


@main.command()
@click.option('--password', prompt='Your Password',
              help='The password used to encrypt and decrypt file.')
@click.argument('filename')
def encrypt(password, filename):
    """Encrypt file"""
    if not os.path.isfile(filename) and not os.path.isdir(filename):
        click.echo('File %s does\'nt exist' % filename)
        return
    is_dir = False
    if os.path.isdir(filename):
        filename = zip_dir(filename)
        is_dir = True
    if not write_hash_file(filename):
        click.echo('Error while writing hash file, check your permission')
    cryptfile = filename + CRYPT_EXT
    pyAesCrypt.encryptFile(filename, cryptfile, password, BUFFER_SIZE)
    click.echo('File was encrypted, hash file was generated')
    # click.echo('Removing original file')
    if is_dir:
        os.remove(filename)


@main.command()
@click.option('--password', prompt='Your Password',
              help='The password used to encrypt and decrypt file.')
@click.argument('filename')
def decrypt(password, filename):
    """Decrypt file"""
    if not os.path.isfile(filename):
        click.echo('File %s does\'nt exist' % filename)
        return
    if os.path.isdir(filename):
        filename = zip_dir(filename)
    decryptfile = filename.replace(CRYPT_EXT, '')
    pyAesCrypt.decryptFile(filename, decryptfile, password, BUFFER_SIZE)
    if check_hash_file(decryptfile):
        click.echo(
            'File decrypted, Hash was the same\nRemoving hash and cryptfile')
        os.remove(decryptfile + SHA256SUM_EXT)
        os.remove(filename)
        if is_zip(decryptfile):
            unzip(decryptfile)
            os.remove(decryptfile)
        return
    click.echo('Hash are not the same, file may be corrupted')


def is_zip(filename):
    base = os.path.basename(filename)
    ext = os.path.splitext(base)[1]
    return ext == ARCHIVE_EXT


def zip_dir(dir):
    archive = dir + ARCHIVE_EXT
    with zipfile.ZipFile(archive, "w") as zf:
        for dirname, subdirs, files in os.walk(dir):
            zf.write(dirname)
            for filename in files:
                zf.write(os.path.join(dirname, filename))
    return archive


def unzip(archive):
    base = os.path.basename(archive)
    dir_name = os.path.splitext(base)[0]
    with zipfile.ZipFile(archive, 'r') as zf:
        zf.extractall(dir_name)


def check_hash_file(filename):
    hash_file = filename + SHA256SUM_EXT
    with open(hash_file, 'r') as f:
        content = f.read().split()
        correct_hash = content[0]
        hashed = sha256sum(filename)
        if correct_hash == hashed:
            return True
    return False


def after_decrypt(filename):
    if check_hash_file(filename):
        print("File hashes match!\nRemoving hashfile")
        hash_file = filename + SHA256SUM_EXT
        os.remove(hash_file)
    print("Hash don't match, file may be corrupted")


def write_hash_file(filename):
    hash_file = filename + SHA256SUM_EXT
    hashed = sha256sum(filename)
    if hashed == NON_EXISTENT:
        return False
    with open(hash_file, 'w') as f:
        f.write(hashed + " " + filename + "\n")
        return True
    return False


def sha256sum(filename):
    with open(filename, 'rb') as f:
        bytes = f.read()  # read entire file as bytes
        readable_hash = hashlib.sha256(bytes).hexdigest()
        return readable_hash
    return NON_EXISTENT


if __name__ == '__main__':
    main()
